from classifier import ImageClassifier

model_name = 'kutya_cica_model'

# train a model
ic = ImageClassifier()
ic.set_classes(['0', '1'])
ic.set_model_name(model_name)
ic.set_train_data_path('data/train')
ic.train(3000)

# use the model to classify images from a different folder
ic.predict('data/predict')
ic.print_results()

# create an html page with a graphic report
ic.print_html_report()