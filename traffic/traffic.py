import random
import numpy as np
import tensorflow as tf

from skimage import transform
from skimage.color import rgb2gray

from loader import *
from visualizer import *

#
# Implementaion of this tutorial: https://www.datacamp.com/community/tutorials/tensorflow-tutorial
#

images, labels = load_train_data()

# Ugyan olyan méretűvé kell varázsolni a képeket!
images28 = [transform.resize(image, (28, 28)) for image in images]

# numpy tömb-be bele
images28 = np.array(images28)

# fekete fehér
images28 = rgb2gray(images28)

# placeholderek: csak a sessionban kapnak valós értéke, csak 'placeholderek'
x = tf.placeholder(dtype = tf.float32, shape = [None, 28, 28])
y = tf.placeholder(dtype = tf.int32, shape = [None])

# Flatten the input data
# A 28x28as képek mátrixát alakítja át [None, 28, 28] -> [None, 784]
images_flat = tf.contrib.layers.flatten(x)

# Fully connected layer
logits = tf.contrib.layers.fully_connected(images_flat, 62, tf.nn.relu)

# Define a loss function
loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y,
                                                                    logits = logits))
# Define an optimizer
train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

# Convert logits to label indexes
correct_pred = tf.argmax(logits, 1)

# Define an accuracy metric
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Pick 10 random images
sample_indexes = random.sample(range(len(images28)), 10)
sample_images = [images28[i] for i in sample_indexes]
sample_labels = [labels[i] for i in sample_indexes]

# Run our network!
tf.set_random_seed(1234)

sess = tf.Session()
# with tf.Session() as sess:
sess.run(tf.global_variables_initializer())
for i in range(201):
    print('EPOCH', i)
    _, accuracy_val = sess.run([train_op, accuracy], feed_dict={x: images28, y: labels})
    if i % 10 == 0:
        print("Loss: ", loss)
    print('DONE WITH EPOCH')

# Run the "correct_pred" operation
predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]


# Print the real and predicted labels
print(sample_labels)
print(predicted)

display_predictions(sample_images, sample_labels, predicted)

# tests:
# Load the test data
test_images, test_labels = load_test_data()

# Transform the images to 28 by 28 pixels
test_images28 = [transform.resize(image, (28, 28)) for image in test_images]

# Convert to grayscale
from skimage.color import rgb2gray
test_images28 = rgb2gray(np.array(test_images28))

# Run predictions against the full test set.
# with tf.Session() as sess:
sess.run(tf.global_variables_initializer())
predicted = sess.run([correct_pred], feed_dict={x: test_images28})[0]

# Calculate correct matches
match_count = sum([int(y == y_) for y, y_ in zip(test_labels, predicted)])

# Calculate the accuracy
maccuracy = match_count / len(test_labels)


# Print the accuracy
print("Accuracy: {:.3f}".format(maccuracy))
sess.close()





