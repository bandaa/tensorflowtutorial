import os
import skimage
from skimage import data

# NOTE: load the datas from here: https://btsd.ethz.ch/shareddata/


ROOT_PATH = "C:\\Users\\Projects\\resources\\traffic"
train_data_directory = os.path.join(ROOT_PATH, "BelgiumTSC_Training/Training")
test_data_directory = os.path.join(ROOT_PATH, "BelgiumTSC_Testing/Testing")

def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory)
                   if os.path.isdir(os.path.join(data_directory, d))]
    labels = []
    images = []
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names = [os.path.join(label_directory, f)
                      for f in os.listdir(label_directory)
                      if f.endswith(".ppm")]
        for f in file_names:
            images.append(skimage.data.imread(f))
            labels.append(int(d))
    return images, labels

def load_train_data():
    return load_data(train_data_directory)

def load_test_data():
    return load_data(test_data_directory)


